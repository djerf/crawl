var url = new Map();

function parseURL(inputLink: string) {
  let regex = /(\w+:)(\/\/)?(.*@)?(.*\.\w+)(:\d+)?(\/[^\?]*)?(\?[^#]*)?(#.*)?/g
  var match = regex.exec(inputLink);
  url.set('scheme', match[1]);
  url.set('credentials', match[3]);
  url.set('domain', match[4]);
  url.set('port', match[5]);
  url.set('path', match[6]);
  url.set('query', match[7]);
  url.set('anchor', match[8]);
  console.log(['input link: ', inputLink].join(''));
  console.log(url);
}

parseURL('https://user:password@mail.google.com:567/path/sub?query#anchor');
parseURL('http://www.google.com');
parseURL('mailto:brian@eaiti.com');