import axios from 'axios';

var visitedSites = new Map();

async function crawl(inputLink: string, depthLimit: number, pageLimit: number) {

  if (depthLimit <= 0 || visitedSites.get(inputLink)) return;

  visitedSites.set(inputLink, true);
  console.log(inputLink);

  await findLinks(inputLink, pageLimit).then(list => {
    list.forEach((link) => crawl(link, depthLimit - 1, pageLimit));
  });
}

async function findLinks(inputLink: string, pageLimit: number): Promise<string[]> {
  try {
    return await axios.get(inputLink).then(response => {
      let re = /(?<=a\shref=")([^"%]*)/g;
      return response.data.match(re).slice(0, pageLimit - 1).map((x) => {
        return addContext(x, inputLink) // fix if relative link
      });
    });
  } catch (error) {
    return [];
  }
}

function addContext(link, inputLink) {
  if (!link.match(/^http/g))
    return [inputLink, link].join('');
  return link;
}

crawl('https://en.wikipedia.org/wiki/Illuminati', 4, 20);